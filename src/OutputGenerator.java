import java.util.ArrayList;


class OutputGenerator implements Operation{
	private ArrayList<String> listToBeDisplay;
	private StringBuilder result;
	
	public OutputGenerator(ArrayList<String> listAfterAlphabetizer) {
		listToBeDisplay = listAfterAlphabetizer;
	}

	public void execute() {
		result = new StringBuilder();
		for (int i=0; i<listToBeDisplay.size(); i++) {
			result.append(listToBeDisplay.get(i) + "\n");
		}
	}

	public String getStringToDisplay(){
		return result.toString();
	}
}
