import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.TreeSet;

public class MasterController extends JPanel implements ActionListener {

	private static GUI gui;
	TreeSet<String> listOfWordsToIgnore = new TreeSet<String>();
	ArrayList<String> listOfTitle = new ArrayList<String>();
	ArrayList<String> results = new ArrayList<String>();
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGui();
				}
			});
	}

	private static void createAndShowGui() {
		gui = new GUI();
		JFrame frameKWIC = new JFrame("Key Word In Context");
		frameKWIC.getContentPane().add(gui);
 
		frameKWIC.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameKWIC.setSize(1000, 550);
		frameKWIC.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		ArrayList<String> input = gui.getUserInput();
		listOfWordsToIgnore.clear();
		listOfTitle.clear();
		results.clear();
		
		executeChangeInputFormat(input);
		executeGetTitleShiftAndResult();
		executeSortResults();
		String finalResultToBeDisplayed = executeConvertResultToString();
		executeShowResultsInGUI(finalResultToBeDisplayed);
	}
	
	// call InputGenerator class
	private void executeChangeInputFormat(ArrayList<String> input) {
		InputGenerator inputGenerator = new InputGenerator(input);
		inputGenerator.execute();
		listOfTitle = inputGenerator.getListOfTitle();
		listOfWordsToIgnore = inputGenerator.getListOfWordToIgnore();
	}
	
	// call CircularShifter class
	private void executeGetTitleShiftAndResult() {
		CircularShifter shifter = new CircularShifter(listOfWordsToIgnore);
		for (int i=0; i<listOfTitle.size(); i++) {
			shifter.set(listOfTitle.get(i));
			shifter.execute();
			results.addAll(shifter.getResult());
		}
		
	}
	
	// call Alphabetizer class
	private void executeSortResults() {
		Alphabetizer alphabetizer = new Alphabetizer(results);
		alphabetizer.execute();
		results = alphabetizer.getAlphabetizedList();
	}
	
	// call OutputGenerator class
	private String executeConvertResultToString() {
		OutputGenerator generator = new OutputGenerator(results);
		generator.execute();
		
		return generator.getStringToDisplay();
	}
	
	// call GUI
	private void executeShowResultsInGUI(String result) {
		gui.setOutput(result);
	}
}
