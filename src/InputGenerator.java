import java.util.TreeSet;
import java.util.ArrayList;

class InputGenerator implements Operation{
	private String rawDataOfTitle;
	private String rawDataOfWordToIgnore;
	private ArrayList<String> listOfTitle = new ArrayList<String>();
	private TreeSet<String> listOfWordToIgnore = new TreeSet<String>();
	
	public InputGenerator(ArrayList<String> input) {
		this.set(input);
	}
	
	public void set(ArrayList<String> input) {
		this.rawDataOfTitle = input.get(1).trim();
		this.rawDataOfWordToIgnore = input.get(0).trim();
	}
	
	public void execute() {
		String[] tempListOfTitle = rawDataOfTitle.split("\\r?\\n");
		String[] tempListOfWordToIgnore = rawDataOfWordToIgnore.split(" ");

		for(int i = 0; i < tempListOfTitle.length; i++) {
			if (!tempListOfTitle[i].trim().equals("")) {
				listOfTitle.add(tempListOfTitle[i].toLowerCase());
			}
		}
		
		for(int i = 0; i < tempListOfWordToIgnore.length; i++) {
			if (!tempListOfWordToIgnore[i].trim().equals("")) {
				listOfWordToIgnore.add(tempListOfWordToIgnore[i].toLowerCase());
			}
		}
	}
	
	public ArrayList<String> getListOfTitle() {
		return listOfTitle;
	}
	
	public TreeSet<String> getListOfWordToIgnore() {
		return listOfWordToIgnore;
	}
}
