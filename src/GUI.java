import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

public class GUI extends JPanel{

	private static final Font font = new Font("Verdana", Font.BOLD, 20);
	private JTextField wordsToIgnore = new JTextField();
	private JTextArea context = new JTextArea();
	JTextArea output = new JTextArea();
	JLabel labelOutput = new JLabel();
	JScrollPane scrollContext = new JScrollPane(context);
	JScrollPane scrollOutput = new JScrollPane(output);
	
	public GUI(){
		
		
		//add word to ignore label
		JLabel labelWordsToIgnore = new JLabel();
		labelWordsToIgnore.setText("Enter words to ignore (Please seperate by space)");
		labelWordsToIgnore.setBounds(10, 10, 450, 25);
		add(labelWordsToIgnore);
		
		//add word to ignore text field
		wordsToIgnore.setBounds(10, 35, 450, 25);
		wordsToIgnore.setBorder(BorderFactory.createLineBorder(Color.gray,1));
		wordsToIgnore.setFont(font);
		add(wordsToIgnore);
		
		//add context label
		JLabel labelContext = new JLabel();
		labelContext.setText("Enter context (One sentence per line)");
		labelContext.setBounds(10, 65, 450, 25);
		add(labelContext);
		
		//add context text area
		context.setBounds(10, 90, 450, 300);
		context.setBorder(BorderFactory.createLineBorder(Color.gray,1));
		context.setFont(font);
		context.setLineWrap(true);
		context.setWrapStyleWord(true);
		//add(context);
		
		scrollContext.setBounds(10, 90, 450, 300);
		scrollContext.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		add(scrollContext);
		
		JButton button = new JButton(">> Submit <<");
		button.setBounds(100, 400, 200, 25);
		add(button);
		
		//add output label
		labelOutput.setText("The result(s):");
		labelOutput.setBounds(500, 10, 450, 25);
		add(labelOutput);
		
		//add output text area
		output.setBounds(500, 35, 450, 450);
		output.setBorder(BorderFactory.createLineBorder(Color.gray,1));
		output.setFont(font);
		output.setLineWrap(true);
		output.setWrapStyleWord(true);
		output.setEditable(false);
		setOutput("");
		
		//add output scroll pane
		scrollOutput.setBounds(500, 35, 450, 450);
		scrollOutput.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		add(scrollOutput);
		
		setLayout(null);
		
		button.addActionListener(new MasterController());
	}
	
	public ArrayList<String> getUserInput() {
		ArrayList<String> input = new ArrayList<String>();
		input.add(wordsToIgnore.getText());
		input.add(context.getText());
		return input;
	}
	
	public void setOutput(String result) {
		//add output text
		output.setText(result);

	}
}
