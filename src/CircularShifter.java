import java.util.ArrayList;
import java.util.TreeSet;

class CircularShifter implements Operation {
	private String currentString;
	private String initialString;
	private TreeSet<String> wordsToIgnore;
	private ArrayList<String> results;
	
	public CircularShifter(TreeSet<String> input) {
		wordsToIgnore = input;
		results = new ArrayList<String>();
	}
	
	public void set(String str) {
		initialString = str.trim();
		currentString = new String();
		results.clear();
	}
	
	public void execute() {
		capitalizeKeyWords();
		do {
			shiftFirstWordtoLast();
		} while (!currentString.equalsIgnoreCase(initialString));
	}
	
	private void capitalizeKeyWords() {
		String[] words = initialString.split(" ");
		String afterRemoveSpace = new String();
		for (int i=0; i<words.length; i++) {
			if (words[i].equals("")) continue;
			afterRemoveSpace += words[i] + " ";
		}
		
		initialString = afterRemoveSpace.trim();
		words = initialString.split(" ");
		
		for (int i=0; i<words.length; i++) {
			if (wordsToIgnore.contains(words[i].toLowerCase())) {
				words[i] = words[i].toLowerCase();
			} else {
				words[i] = words[i].substring(0, 1).toUpperCase() + words[i].substring(1).toLowerCase();
			}
			currentString += words[i] + " ";
		}
		
		currentString = currentString.trim();
	}
	
	private void shiftFirstWordtoLast() {
		int index = currentString.indexOf(" ");
		String firstWord;
		
		if (index > 0) {
			firstWord = currentString.substring(0, index);
		} else {
			firstWord = currentString;
		}
		
		if (!wordsToIgnore.contains(firstWord.toLowerCase())) {
			results.add(currentString);
		}
		
		if (index > 0) {
			currentString = currentString.substring(index+1).trim();
			currentString += " " + firstWord;
		}
	}
	
	public ArrayList<String> getResult() {
		return results;
	}
}
