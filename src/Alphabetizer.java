import java.util.ArrayList;
import java.util.Collections;


class Alphabetizer implements Operation{
	private ArrayList<String> listToBeAlphabetize;
	
	public Alphabetizer(ArrayList<String> listAfterCircularShift) {
		this.set(listAfterCircularShift);
	}
	
	public void set(ArrayList<String> object) {
		this.listToBeAlphabetize = object;	
	}
	
	public void execute() {
		alphabetizeList();
	}
	
	private void alphabetizeList() {
		Collections.sort(listToBeAlphabetize);
	}
	
	public ArrayList<String> getAlphabetizedList() {
		return this.listToBeAlphabetize;
	}
}
